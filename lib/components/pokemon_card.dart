import 'package:flutter/material.dart';
import 'package:pokeshoww/model/pokemon.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';
import 'package:pokeshoww/views/app_view.dart';

class PokemonCard extends StatelessAppView {
  final Pokemon pokemon;
  final void Function() onClick;

  const PokemonCard(
      {Key? key,
      required this.pokemon,
      required this.onClick,
      AppProvider? provider})
      : super(key: key, provider: provider);

  @override
  Widget build(BuildContext context) {
    return _makeContent(context: context);
  }

  Widget _makeContent({required BuildContext context}) {
    return Container(
      padding: const EdgeInsets.all(10.0),
      child: InkWell(
          onTap: onClick,
          child: Card(
              elevation: 10.0,
              margin: const EdgeInsets.fromLTRB(20, 10, 20, 10),
              shape: const RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(20.0))),
              child: Center(
                child: Container(
                  padding: const EdgeInsets.all(15),
                  margin: const EdgeInsets.all(10),
                  child: Row(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Center(
                          child: Image(
                              image: NetworkImage(pokemon.image()),
                              height: 90.0 *
                                  MediaQuery.of(context)
                                      .copyWith()
                                      .textScaleFactor),
                        ),
                      ),
                      Expanded(
                        flex: 2,
                        child: Center(
                            child: Container(
                          margin: const EdgeInsets.all(15),
                          padding: const EdgeInsets.all(10),
                          alignment: Alignment.center,
                          child: Text(pokemon.name!.toUpperCase(),
                              textScaleFactor: 0.7,
                              style: TextStyle(
                                  fontSize: 29 *
                                      MediaQuery.of(context)
                                          .copyWith()
                                          .textScaleFactor)),
                        )),
                      ),
                    ],
                  ),
                ),
              ))),
    );
  }
}
