import 'package:flutter/material.dart';

/// TODO: this class can be refactored to accept a widget
/// and not a String like text
class InfoCard extends StatelessWidget {
  final double elevation;
  final Widget body;
  final Icon icon;
  final Function onPress;
  final EdgeInsets? padding;
  final EdgeInsets? margin;

  const InfoCard({
    Key? key,
    required this.icon,
    required this.elevation,
    required this.body,
    required this.onPress,
    this.padding,
    this.margin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _makeContent(context: context);
  }

  Widget _makeContent({required BuildContext context}) {
    return Card(
      elevation: elevation,
      shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20.0))),
      child: Container(
          padding: padding ?? const EdgeInsets.all(10),
          margin: margin ?? const EdgeInsets.all(10),
          child: Row(
            children: [
              Expanded(flex: 1, child: icon),
              Expanded(flex: 5, child: body),
            ],
          )),
    );
  }
}
