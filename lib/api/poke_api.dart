import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:pokeshoww/api/response_wrapper.dart';
import 'package:pokeshoww/model/pokemon.dart';
import 'package:pokeshoww/model/pokemon_move.dart';

/// PokeAPI is a wrapper around the
/// Pokemon API, it make the call to the server
/// and return the object mapping ready to use
/// it in the UI
class PokeAPI {
  final String baseUrl;
  final String version;
  int pokemonSize = -1;

  PokeAPI({this.baseUrl = "https://pokeapi.co", this.version = "api/v2"});

  void checkResponse(http.Response response) {
    if (response.statusCode != 200) {
      throw Exception(
          "Error from API with error ${response.statusCode} and message ${response.body}");
    }
  }

  /// Query the API with the paginator information to get all the information
  /// that the app need to show the pokemon
  Future<List<Pokemon>> pokemonList({int offset = 20, int limit = 20}) async {
    var url =
        Uri.parse("$baseUrl/$version/pokemon?offset=$offset&limit=$limit");
    var response = await http.get(url);
    checkResponse(response);
    var rawResp = PaginatorWrapper.fromJson(jsonDecode(response.body));
    if (pokemonSize < 0) {
      pokemonSize = rawResp.count!;
    }
    // FIXME we are missing here if the offset it is not grater than the tot pokemon
    if (pokemonSize < offset + limit) {
      return [];
    }
    List<Pokemon> pokemonList = List.empty(growable: true);
    if (rawResp.results != null) {
      for (var rawPokemon in rawResp.results!) {
        var name = rawPokemon["name"];
        var pokeUrl = rawPokemon["url"]!;
        var pokemon = await getPokemon(name: name, url: pokeUrl);
        pokemonList.add(pokemon);
      }
    } else {
      throw Exception("results is null in the body: ${response.body}");
    }
    return pokemonList;
  }

  /// Get the information about the single pokemon
  /// that include multiple API call
  Future<Pokemon> getPokemon(
      {required String name, required String url}) async {
    var response = await http.get(Uri.parse(url));
    checkResponse(response);
    var pokemon = Pokemon.fromJson(jsonDecode(response.body));
    pokemon.cityName = await getNameCity(url: pokemon.locationArea);
    //pokemon.resolvedMoves = await getPokemonMoves(moves: pokemon.moves!);
    pokemon.name = name;
    return pokemon;
  }

  /// Call to get the information around of the city, and in particular
  /// about the city name. So the result here is the city name.
  Future<String> getNameCity({required String? url}) async {
    var response = await http.get(Uri.parse(url!));
    List<dynamic> rawJson = jsonDecode(response.body);
    if (rawJson.isEmpty) {
      return "unknown";
    }
    return rawJson[0]["location_area"]["name"]!
        .toString()
        .toUpperCase()
        .replaceAll("-", " ");
  }

  /// get all the information about a move and return back the new list of pokemon move
  /// FIXME: This can be optimized to use only a single list, but for now we keep the code simple
  Future<List<PokemonMove>> getPokemonMoves(
      {required List<MapMove> moves}) async {
    List<PokemonMove> result = List.empty(growable: true);
    for (var urlMove in moves) {
      if (urlMove.move.url == null) {
        continue;
      }
      var response = await http.get(Uri.parse(urlMove.move.url!));
      checkResponse(response);
      var pokemonMove = PokemonMove.fromJson(jsonDecode(response.body));
      result.add(pokemonMove);
    }
    return result;
  }
}
