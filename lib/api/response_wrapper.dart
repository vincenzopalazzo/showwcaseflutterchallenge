import 'package:json_annotation/json_annotation.dart';

part 'response_wrapper.g.dart';

/// Simple paginator response wrapper to make the
/// code readable
@JsonSerializable()
class PaginatorWrapper {
  int? count;
  String? previous;
  List<Map<String, dynamic>>? results;

  PaginatorWrapper();

  factory PaginatorWrapper.fromJson(Map<String, dynamic> json) =>
      _$PaginatorWrapperFromJson(json);
  Map<String, dynamic> toJson() => _$PaginatorWrapperToJson(this);
}
