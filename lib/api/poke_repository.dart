import 'package:hive_flutter/hive_flutter.dart';
import 'package:pokeshoww/model/pokemon.dart';

part 'poke_repository.g.dart';

/// This is a pokemon repository that contains
/// a mock repository of Pokemon.
///
/// In particular, the rest api is a static API, so it
/// doesn't support the add pokemon operation, so
/// with this repository implement a mock that use hive
/// to make the information persistent.
///
/// author: https://github.com/vincenzopalazzo
class PokeRepository {
  PokemonRepository? repository;
  Box? box;

  Future<void> init() async {
    var repositoryKey = "repositoryKey";
    await Hive.initFlutter();
    Hive.registerAdapter(PokemonRepositoryAdapter());
    Hive.registerAdapter(PokemonAdapter());
    box = await Hive.openBox(repositoryKey);
  }

  void checkAndRestore() async {
    if (repository == null) {
      var persistence = box!.get("repository1");
      if (persistence == null) {
        repository = PokemonRepository();
        box!.put("repository1", repository);
      } else {
        repository = persistence;
      }
    }
  }

  void addPokemon(Pokemon pokemon) {
    checkAndRestore();
    repository!.addPokemon(pokemon);
    box!.put("repository1", repository);
  }

  Pokemon? getPokemon({required String name}) {
    checkAndRestore();
    var pokemon = repository!.getPokemon(name: name);
    return pokemon;
  }

  List<Pokemon> getPokemonList() {
    checkAndRestore();
    return repository!.pokemonCreated;
  }
}

@HiveType(typeId: 1)
class PokemonRepository extends HiveObject {
  @HiveField(0)
  // TODO: Hive has some problem with Set, but here
  // the correct data structure is the set
  late List<Pokemon> _pokemonCreated;

  PokemonRepository() {
    _pokemonCreated = [];
  }

  List<Pokemon> get pokemonCreated => _pokemonCreated.toList();

  void addPokemon(Pokemon pokemon) {
    _pokemonCreated.add(pokemon);
  }

  Pokemon? getPokemon({required String name}) {
    return _pokemonCreated.firstWhere((pokemon) => pokemon.name! == name);
  }
}
