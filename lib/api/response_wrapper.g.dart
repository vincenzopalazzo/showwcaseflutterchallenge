// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'response_wrapper.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PaginatorWrapper _$PaginatorWrapperFromJson(Map<String, dynamic> json) =>
    PaginatorWrapper()
      ..count = json['count'] as int?
      ..previous = json['previous'] as String?
      ..results = (json['results'] as List<dynamic>?)
          ?.map((e) => e as Map<String, dynamic>)
          .toList();

Map<String, dynamic> _$PaginatorWrapperToJson(PaginatorWrapper instance) =>
    <String, dynamic>{
      'count': instance.count,
      'previous': instance.previous,
      'results': instance.results,
    };
