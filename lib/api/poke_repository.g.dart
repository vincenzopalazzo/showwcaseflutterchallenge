// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'poke_repository.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PokemonRepositoryAdapter extends TypeAdapter<PokemonRepository> {
  @override
  final int typeId = 1;

  @override
  PokemonRepository read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return PokemonRepository()
      .._pokemonCreated = (fields[0] as List).cast<Pokemon>();
  }

  @override
  void write(BinaryWriter writer, PokemonRepository obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj._pokemonCreated);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PokemonRepositoryAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
