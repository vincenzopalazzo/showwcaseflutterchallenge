import 'package:flutter/material.dart';
import 'package:pokeshoww/api/poke_repository.dart';
import 'package:pokeshoww/model/pokemon.dart';
import 'package:pokeshoww/utils/app_utils.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';
import 'package:pokeshoww/utils/di/getit_provider.dart';
import 'package:pokeshoww/views/app_view.dart';
import 'package:pokeshoww/views/add/add_pokemon.dart';
import 'package:pokeshoww/views/home/home_view.dart';
import 'package:pokeshoww/views/pokemon/pokemon_view.dart';
import 'package:trash_themes/themes.dart';

Future<void> main() async {
  var provider = GetItProvider();
  await provider.init();
  runApp(MyApp(provider: provider));
}

class MyApp extends StatelessAppView {
  /// The Material theme configuration
  /// wrapped in a well written package
  final TrashTheme theme = DraculaTheme();

  MyApp({Key? key, AppProvider? provider})
      : super(key: key, provider: provider);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    var dracula = theme.makeDarkTheme(context: context);
    return MaterialApp(
        title: 'ShowwPokemon',
        debugShowCheckedModeBanner: false,
        themeMode: ThemeMode.dark,
        darkTheme: dracula,
        routes: {
          "showPokemon": (context) => PokemonView(provider: provider!),
        },
        home: AppView(provider: provider));
  }
}

class AppView extends StatelessAppView {
  const AppView({Key? key, AppProvider? provider})
      : super(key: key, provider: provider);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
          title: const Text('ShowwPokemon'),
          centerTitle: false,
          primary: true,
          elevation: 0,
          leading: Container(),
        ),
        floatingActionButton: FloatingActionButton.extended(
            onPressed: () => _makeBottomDialog(context: context),
            backgroundColor: Theme.of(context).colorScheme.primary,
            foregroundColor: Theme.of(context).colorScheme.onPrimary,
            elevation: 5,
            label: const Text("Create"),
            icon: const Icon(Icons.add)),
        body: HomeView(provider: provider));
  }

  void _makeBottomDialog({required BuildContext context}) {
    showModalBottomSheet<void>(
        context: context,
        isScrollControlled: true,
        shape: const RoundedRectangleBorder(
          borderRadius: BorderRadius.only(
              topLeft: Radius.circular(25.0), topRight: Radius.circular(25.0)),
        ),
        builder: (BuildContext context) {
          return AddPokemonView(
              provider: provider,
              onSubmit: (Pokemon newPokemon) async {
                var repository = provider!.get<PokeRepository>();
                repository.addPokemon(newPokemon);
                Navigator.of(context).pop();
                AppUtils.showSnackBar("New pokemon created!", context: context);
              });
        });
  }
}
