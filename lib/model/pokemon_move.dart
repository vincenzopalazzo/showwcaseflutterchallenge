import 'package:json_annotation/json_annotation.dart';

part 'pokemon_move.g.dart';

@JsonSerializable()
class MapMove {
  final PokemonMove move;

  MapMove(this.move);

  factory MapMove.fromJson(Map<String, dynamic> json) =>
      _$MapMoveFromJson(json);

  Map<String, dynamic> toJson() => _$MapMoveToJson(this);
}

@JsonSerializable()
class PokemonMove {
  final String? name;
  final String? url;
  int? accuracy;
  int? power;

  PokemonMove({required this.name, required this.url});

  factory PokemonMove.fromJson(Map<String, dynamic> json) =>
      _$PokemonMoveFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonMoveToJson(this);
}
