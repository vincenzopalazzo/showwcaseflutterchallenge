// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class PokemonAdapter extends TypeAdapter<Pokemon> {
  @override
  final int typeId = 2;

  @override
  Pokemon read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return Pokemon()
      ..name = fields[0] == null ? 'unkonown' : fields[0] as String?;
  }

  @override
  void write(BinaryWriter writer, Pokemon obj) {
    writer
      ..writeByte(1)
      ..writeByte(0)
      ..write(obj.name);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is PokemonAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Pokemon _$PokemonFromJson(Map<String, dynamic> json) => Pokemon()
  ..name = json['name'] as String?
  ..sprites = json['sprites'] as Map<String, dynamic>?
  ..locationArea = json['location_area_encounters'] as String?
  ..cityName = json['cityName'] as String?
  ..moves = (json['moves'] as List<dynamic>?)
      ?.map((e) => MapMove.fromJson(e as Map<String, dynamic>))
      .toList()
  ..resolvedMoves = (json['resolvedMoves'] as List<dynamic>?)
      ?.map((e) => PokemonMove.fromJson(e as Map<String, dynamic>))
      .toList();

Map<String, dynamic> _$PokemonToJson(Pokemon instance) => <String, dynamic>{
      'name': instance.name,
      'sprites': instance.sprites,
      'location_area_encounters': instance.locationArea,
      'cityName': instance.cityName,
      'moves': instance.moves,
      'resolvedMoves': instance.resolvedMoves,
    };
