// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'pokemon_move.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

MapMove _$MapMoveFromJson(Map<String, dynamic> json) => MapMove(
      PokemonMove.fromJson(json['move'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$MapMoveToJson(MapMove instance) => <String, dynamic>{
      'move': instance.move,
    };

PokemonMove _$PokemonMoveFromJson(Map<String, dynamic> json) => PokemonMove(
      name: json['name'] as String?,
      url: json['url'] as String?,
    )
      ..accuracy = json['accuracy'] as int?
      ..power = json['power'] as int?;

Map<String, dynamic> _$PokemonMoveToJson(PokemonMove instance) =>
    <String, dynamic>{
      'name': instance.name,
      'url': instance.url,
      'accuracy': instance.accuracy,
      'power': instance.power,
    };
