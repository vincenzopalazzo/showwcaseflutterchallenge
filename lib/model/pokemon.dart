import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';
import 'package:pokeshoww/model/pokemon_move.dart';

part 'pokemon.g.dart';

@JsonSerializable()
@HiveType(typeId: 2)
class Pokemon {
  @HiveField(0, defaultValue: "-")
  String? name;
  Map<String, dynamic>? sprites;
  @JsonKey(name: "location_area_encounters")
  String? locationArea;
  String? cityName;
  List<MapMove>? moves;
  List<PokemonMove>? resolvedMoves;

  Pokemon();

  String image() {
    if (sprites == null) {
      // in case the avatar not exist, e.g: pokemon created by user
      return "https://gravatar.com/avatar/1c61f238b2475bcbed8f22a46bb5cf2a?s=400&d=wavatar&r=x";
    }
    var image = sprites!["front_default"];
    return image;
  }

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is Pokemon &&
          runtimeType == other.runtimeType &&
          name == other.name;

  @override
  int get hashCode => name.hashCode;

  factory Pokemon.fromJson(Map<String, dynamic> json) =>
      _$PokemonFromJson(json);

  Map<String, dynamic> toJson() => _$PokemonToJson(this);

  @override
  String toString() {
    return 'Pokemon{name: $name}';
  }
}
