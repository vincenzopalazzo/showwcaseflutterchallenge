import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:pokeshoww/model/pokemon.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';
import 'package:pokeshoww/views/app_view.dart';

class AddPokemonView extends StatefulAppView {
  final void Function(Pokemon) onSubmit;

  const AddPokemonView(
      {Key? key, required this.onSubmit, AppProvider? provider})
      : super(key: key, provider: provider);

  @override
  AddPokemonState createState() => AddPokemonState();
}

class AddPokemonState extends State<AddPokemonView> {
  late TextEditingController _controller;
  final Logger logger = Logger();

  @override
  void initState() {
    super.initState();
    _controller = TextEditingController();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  Widget makeButton(BuildContext context,
      {required Function onPress,
      Icon icon = const Icon(Icons.done),
      required String text,
      bool disabled = false,
      ButtonStyle? style}) {
    return ElevatedButton.icon(
        style: ElevatedButton.styleFrom(
          primary: !disabled
              ? Theme.of(context).colorScheme.primary
              : Theme.of(context).disabledColor,
          elevation: 3,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
        ),
        onPressed: () => disabled ? () {} : onPress(),
        icon: icon,
        label: Text(text));
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height * 0.75,
      child: Column(
        children: [
          const Spacer(),
          Expanded(
              child: Text("Insert the Name of new Pokemon",
                  textScaleFactor: 1.3,
                  style: Theme.of(context).textTheme.bodyText1)),
          Expanded(
            flex: 1,
            child: SizedBox(
                width: MediaQuery.of(context).size.width / 1.3,
                height: 50,
                child: TextField(
                  controller: _controller,
                )),
          ),
          const Spacer(),
          Flexible(
            flex: 2,
            child: SizedBox(
                width: 120,
                height: 50,
                child: makeButton(context, onPress: () {
                  var newPokemon = Pokemon();
                  var name = _controller.text;
                  logger.i("Name selected $name");
                  newPokemon.name = name;
                  widget.onSubmit(newPokemon);
                }, text: "Create".toUpperCase())),
          ),
        ],
      ),
    );
  }
}
