import 'package:flutter/material.dart';
import 'package:pokeshoww/api/poke_api.dart';
import 'package:pokeshoww/components/info_card.dart';
import 'package:pokeshoww/model/pokemon.dart';
import 'package:pokeshoww/model/pokemon_move.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';
import 'package:pokeshoww/views/app_view.dart';

class PokemonView extends StatelessAppView {
  const PokemonView({Key? key, required AppProvider provider})
      : super(key: key, provider: provider);

  Future<List<PokemonMove>> resolvedMove({required Pokemon pokemon}) async {
    if (pokemon.resolvedMoves == null && pokemon.moves != null) {
      var api = provider!.get<PokeAPI>();
      pokemon.resolvedMoves = await api.getPokemonMoves(moves: pokemon.moves!);
    }
    return pokemon.resolvedMoves ?? [];
  }

  @override
  Widget build(BuildContext context) {
    var pokemon = provider!.get<Pokemon>();
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pokemon Details"),
      ),
      body: Container(
        color: Theme.of(context).colorScheme.background,
        child: Column(
          children: [
            Expanded(
                flex: 1,
                child: Center(
                    child: Container(
                  margin:
                      const EdgeInsets.only(left: 10.0, top: 10.0, right: 10.0),
                  child: _buildAvatar(context: context, pokemon: pokemon),
                ))),
            Expanded(
                flex: 1,
                child: Center(
                    child: Text(
                  pokemon.name!.toUpperCase(),
                  style: Theme.of(context).textTheme.headline4,
                  textScaleFactor: 0.7,
                ))),
            Expanded(
              flex: 1,
              child: Center(
                  child: InfoCard(
                      icon: const Icon(Icons.location_city),
                      elevation: 2.0,
                      body: Text(pokemon.cityName ?? "-"),
                      onPress: () => {})),
            ),
            Expanded(
                flex: 4, child: _infoCard(context: context, pokemon: pokemon)),
          ],
        ),
      ),
    );
  }

  Widget _buildAvatar(
      {required BuildContext context, required Pokemon pokemon}) {
    return CircleAvatar(
      backgroundColor: Theme.of(context).colorScheme.primaryVariant,
      radius: 60.0,
      child: CircleAvatar(
        radius: 50.0,
        backgroundImage: NetworkImage(pokemon.image()),
        backgroundColor: Colors.transparent,
      ),
    );
  }

  Widget _buildMoveView(
      {required BuildContext context, required PokemonMove move}) {
    return Row(
      children: [
        Expanded(child: Text(move.name!)),
        Expanded(child: Text("Power ${move.power ?? "-"}")),
        Expanded(child: Text("Accuracy ${move.accuracy ?? "-"}")),
      ],
    );
  }

  Widget _infoCard({required BuildContext context, required Pokemon pokemon}) {
    return Container(
      margin: const EdgeInsets.all(20.0),
      child: Card(
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Expanded(
                  flex: 4,
                  child: SizedBox(
                    child: FutureBuilder<List<PokemonMove>>(
                        future: resolvedMove(pokemon: pokemon),
                        builder: (context,
                            AsyncSnapshot<List<PokemonMove>> snapshot) {
                          if (snapshot.hasData) {
                            var moves = snapshot.data ?? [];
                            return CustomScrollView(slivers: [
                              SliverList(
                                  delegate: SliverChildBuilderDelegate(
                                (BuildContext context, int index) {
                                  return Center(
                                      child: InfoCard(
                                          icon: const Icon(Icons.event),
                                          elevation: 2.0,
                                          body: _buildMoveView(
                                              context: context,
                                              move: moves[index]),
                                          onPress: () => {}));
                                },
                                childCount: moves.length,
                              ))
                            ]);
                          } else {
                            return const Text("Loading ..");
                          }
                        }),
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
