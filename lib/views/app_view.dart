import 'package:flutter/material.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';

/// Generic App View that encapsulate the App provider
/// in a stateless widget.
abstract class StatelessAppView extends StatelessWidget {
  final AppProvider? provider;

  const StatelessAppView({Key? key, required this.provider}) : super(key: key);
}

abstract class StatefulAppView extends StatefulWidget {
  final AppProvider? provider;

  const StatefulAppView({Key? key, required this.provider}) : super(key: key);
}
