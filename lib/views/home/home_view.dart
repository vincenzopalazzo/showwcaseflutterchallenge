import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:pokeshoww/api/poke_api.dart';
import 'package:pokeshoww/api/poke_repository.dart';
import 'package:pokeshoww/components/pokemon_card.dart';
import 'package:pokeshoww/model/pokemon.dart';
import 'package:pokeshoww/utils/app_utils.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';
import 'package:pokeshoww/views/app_view.dart';

import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:material_floating_search_bar/material_floating_search_bar.dart';

class HomeView extends StatefulAppView {
  const HomeView({Key? key, AppProvider? provider})
      : super(key: key, provider: provider);

  @override
  HomeViewState createState() => HomeViewState();
}

class HomeViewState extends State<HomeView> {
  final int _pageSize = 20;
  final Logger _logger = Logger();

  final PagingController<int, Pokemon> _pagingController =
      PagingController(firstPageKey: 0);

  @override
  void initState() {
    _pagingController.addPageRequestListener((pageKey) async {
      var api = widget.provider!.get<PokeAPI>();
      var repo = widget.provider!.get<PokeRepository>();
      AppUtils.fetchInPaginatorUI(
          pagingController: _pagingController,
          repository: repo,
          api: api,
          limit: pageKey,
          pageSize: _pageSize,
          logger: _logger);
    });
    super.initState();
  }

  @override
  void dispose() {
    _pagingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Container(
            color: Theme.of(context).colorScheme.background,
            child: Stack(
              fit: StackFit.expand,
              children: [
                _buildHomeView(context),
                _buildFloatingSearchBar(context),
              ],
            )));
  }

  Widget _buildHomeView(BuildContext context) {
    return Column(
      children: [
        const Spacer(),
        Expanded(
          flex: 5,
          child: PagedListView<int, Pokemon>(
            primary: true,
            pagingController: _pagingController,
            builderDelegate: PagedChildBuilderDelegate<Pokemon>(
              itemBuilder: (context, item, index) => PokemonCard(
                  pokemon: item,
                  onClick: () {
                    widget.provider!.registerDependence<Pokemon>(item);
                    Navigator.pushNamed(context, "showPokemon");
                  }),
            ),
          ),
        )
      ],
    );
  }

  /// Build the search bar.
  Widget _buildFloatingSearchBar(BuildContext context) {
    final isPortrait =
        MediaQuery.of(context).orientation == Orientation.portrait;

    return FloatingSearchBar(
      backgroundColor: Theme.of(context).cardColor,
      hint: 'Search by Pokemon Name',
      scrollPadding: const EdgeInsets.only(top: 16, bottom: 56),
      transitionDuration: const Duration(milliseconds: 800),
      transitionCurve: Curves.easeInOut,
      physics: const BouncingScrollPhysics(),
      axisAlignment: isPortrait ? 0.0 : 0.0,
      openAxisAlignment: 0.0,
      width: isPortrait ? 600 : 500,
      debounceDelay: const Duration(milliseconds: 500),
      onQueryChanged: (query) {
        // Call your model, bloc, controller here.
      },
      // Specify a custom transition to be used for
      // animating between opened and closed stated.
      transition: CircularFloatingSearchBarTransition(),
      actions: [
        FloatingSearchBarAction(
          showIfOpened: false,
          child: IconButton(
            icon: const Icon(Icons.close),
            onPressed: () {},
          ),
        ),
        FloatingSearchBarAction.searchToClear(
          showIfClosed: false,
        ),
      ],
      builder: (context, transition) {
        return ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Material(
            color: Colors.white,
            elevation: 4.0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: const [Text("TODO to be implemented")],
            ),
          ),
        );
      },
    );
  }
}
