import 'package:flutter/material.dart';
import 'package:infinite_scroll_pagination/infinite_scroll_pagination.dart';
import 'package:logger/logger.dart';
import 'package:pokeshoww/api/poke_api.dart';
import 'package:pokeshoww/api/poke_repository.dart';

class AppUtils {
  static Future<void> fetchInPaginatorUI(
      {required PagingController pagingController,
      required PokeRepository repository,
      required PokeAPI api,
      required int limit,
      required int pageSize,
      required Logger logger}) async {
    try {
      var logger = Logger();
      var pokemons = await api.pokemonList(offset: limit, limit: pageSize);
      // Is the first page, so we need to add only one times
      // the custom pokemon on the list
      var localPokemon = repository.getPokemonList();
      logger.d(localPokemon);
      pokemons.addAll(localPokemon);
      if (pokemons.isEmpty) {
        pagingController.appendLastPage(pokemons);
        return;
      }
      pagingController.appendPage(pokemons, limit + pokemons.length);
    } catch (error, stacktrace) {
      logger.e(error);
      logger.e(stacktrace);
      pagingController.error = error;
    }
  }

  static void showSnackBar(String message,
      {required BuildContext context, Action? action, String label = "Close"}) {
    var snackBar = SnackBar(
        behavior: SnackBarBehavior.fixed,
        backgroundColor: Theme.of(context).selectedRowColor,
        content: Text(message),
        action: SnackBarAction(
            label: label,
            textColor: Theme.of(context).textTheme.bodyText1!.color!,
            onPressed: () =>
                ScaffoldMessenger.of(context).hideCurrentSnackBar()));
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }
}
