/// Interface implementation of App provider, this will help us
/// to wrapper any provider library and to decupled the app from any
/// external dependencies
abstract class AppProvider {
  /// Method to register an instance inside the provider
  void registerDependence<T extends Object>(T implementation,
      {bool eager = false, bool override = true});

  /// Method to get an instance inside the provider
  T get<T extends Object>();

  /// Main init method to initialize the Provider of the application
  /// this will call from the framework
  Future<void> init();
}
