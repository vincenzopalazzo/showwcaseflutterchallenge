import 'package:get_it/get_it.dart';
import 'package:pokeshoww/api/poke_api.dart';
import 'package:pokeshoww/api/poke_repository.dart';
import 'package:pokeshoww/utils/di/app_provider.dart';

/// Specific implementation of the App Provider where the user can
/// init the dependencies with the get_it package.
///
/// and the method is called by the user
class GetItProvider extends AppProvider {
  @override
  void registerDependence<T extends Object>(T implementation,
      {bool eager = false, bool override = true}) {
    if (GetIt.instance.isRegistered<T>() && override == true) {
      GetIt.instance.unregister<T>();
    }
    GetIt.instance.registerSingleton<T>(implementation);
  }

  @override
  T get<T extends Object>() => GetIt.instance.get<T>();

  @override
  Future<GetItProvider> init() async {
    registerDependence<PokeAPI>(PokeAPI());
    var repository = PokeRepository();
    await repository.init();
    registerDependence<PokeRepository>(repository);
    return this;
  }
}
