CC=flutter
FMT=format

default: fmt

fmt:
	$(CC) $(FMT) .
	$(CC) analyze .

check:
	$(CC) test

gen:
	$(CC) pub run build_runner build

run:
	$(CC) run -d chrome